console.log("hello world");

let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];
// let userIndex = users.length-1;

console.log("Original Array:")
console.log(users);

/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
    function reAssignWrestle(index){
          console.log(users[index]);
      }

      users[users.length] = "john cena";
      console.log(users);

      reAssignWrestle(2);
   
      reAssignWrestle(users.length-1);
      
      users.length = users.length-1;
      console.log(users);

      users[3] = "Triple H";
      console.log(users);



    // function nullArray(){
    //     let emptyArray = [];
    // }
   

 
      




/*
S21 Activity Instruction:
1. In the S21 folder, create an a1 folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code and instructions from your Boodle Notes into your index.js.
4. Create a function which is able to receive a single argument and add the input at the end of the users array.
- function should be able to receive a single argument.
- add the input data at the end of the array.
- The function should not be able to return data.
- invoke and add an argument to be passed in the function.
- log the users array in the console.
5. Create a function which is able to receive an index number as a single argument return the item accessed by its index.
- function should be able to receive a single argument.
- return the item accessed by the index.
- Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
~ log the itemFound variable in the console.
6. Create a function which is able to delete the last item in the array and return the deleted item.
- create a function scoped variable to store the last item in the users array.
- shorten the length of the array by at least 1 to delete the last item.
- return the last item in the array which was stored in the function scoped variable.
- create a global scoped variable outside of the function and store the result of the function.
~ log the global scoped variable in the console.
7. Create a function which is able to update a specific item in the array by its index.
- function should be able to receive 2 arguments, the update and the index number.
- first, access and locate the item by its index then re-assign the item with the update.
- this function should not have a return.
- invoke the function and add the update and index number as arguments.
- outside of the function, log the users array in the console.
8. Create a function which is able to delete all items in the array.
- you can modify/set the length of the array.
- the function should not return anything.
- invoke the function.
- outside of the function, Log the users array in the console.


9. Create a function which is able to check if the array is empty.
- add an if statement to check if the length of the users array is greater than 0.
- if it is, return false.
- else, return true.
- create a global variable called outside of the function called isUsersEmpty and store the returned value from the function.*/

  function usersArray(array){
    console.log('[]')
    if(array > 0){
        return 'false';
    }
        else{
            return 'true'
        }
    }



let isUserEmpty = usersArray([]);
console.log(isUserEmpty);
        





/*

~ log the isUsersEmpty variable in the console.
10. Create a git repository named S21.
11. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
12. Add the link in Boodle.
*/